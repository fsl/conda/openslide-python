# Conda recipe for openslide-python

This recipe is hosted at https://git.fmrib.ox.ac.uk/fsl/conda/openslide-python.

It builds the `openslide-python` library, hosted at
https://github.com/openslide/openslide-python


```
conda build -c conda-forge --output-folder=<path-to-channel-directory> .
```
